package com.example.prigui.skyproject;

import com.example.prigui.skyproject.model.SkyResponseModel;

import java.util.List;

/**
 * Created by Indigo on 3/5/17.
 */

public interface SkyContract {

    interface View {

        void onFetchDataStarted();

        void onFetchDataCompleted();

        void onFetchDataSuccess(List<SkyResponseModel> skyResponseModel);

        void onFetchDataError(Throwable e);
    }

    interface Presenter {

        void loadData();

        void subscribe();

        void unsubscribe();

        void onDestroy();

    }
}
