package com.example.prigui.skyproject;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;

/**
 * Created by prigui on 24/07/2018.
 */

public class LineHolder {
    private String title;
    private String thumbnail;

    public String getTitle() {
        return title;
    }


    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}