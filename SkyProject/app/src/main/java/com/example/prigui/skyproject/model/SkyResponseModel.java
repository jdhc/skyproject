package com.example.prigui.skyproject.model;

import com.example.prigui.skyproject.model.validator.IsDefined;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Indigo on 2/28/17.
 */

public class SkyResponseModel {

    @IsDefined
    private final String title;

    private final String overview;
    private final String duration;

    @SerializedName("release_year")
    private final String releaseYear;

    @SerializedName("cover_url")
    private final String coverUrl;

    @SerializedName("backdrops_url")
    private final List<String> backdropsUrl;

    private final String id;



    public SkyResponseModel(String title, String overview,
                            String duration, String releaseYear,
                            String coverUrl, List<String> backdropsUrl, String id) {
        this.title = title;
        this.overview = overview;
        this.duration = duration;
        this.releaseYear = releaseYear;
        this.coverUrl = coverUrl;
        this.id = id;
        this.backdropsUrl = backdropsUrl;
    }

    public String getTitle() {
        return title;
    }



    public String getOverview() {
        return overview;
    }

    public String getDuration() {
        return duration;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public List<String> getbackdropsUrl() {
        return backdropsUrl;
    }

    public String getId() {
        return id;
    }
}
