package com.example.prigui.skyproject;

import android.support.annotation.NonNull;

import com.example.prigui.skyproject.model.SkyResponseModel;

import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Scheduler;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Indigo on 3/5/17.
 */

public class SkyPresenter implements SkyContract.Presenter {

    @NonNull
    private CharactersDataSource charactersDataSource;

    @NonNull
    private Scheduler backgroundScheduler;

    @NonNull
    private Scheduler mainScheduler;

    @NonNull
    private CompositeSubscription subscriptions;

    private SkyContract.View view;

    public SkyPresenter(
        @NonNull CharactersDataSource charactersDataSource,
        @NonNull Scheduler backgroundScheduler,
        @NonNull Scheduler mainScheduler,
        SkyContract.View view) {
        this.charactersDataSource = charactersDataSource;
        this.backgroundScheduler = backgroundScheduler;
        this.mainScheduler = mainScheduler;
        this.view = view;

        subscriptions = new CompositeSubscription();
    }



    @Override
    public void loadData() {
        view.onFetchDataStarted();
        subscriptions.clear();

        Subscription subscription = charactersDataSource
                .getCharacters()
                .subscribeOn(backgroundScheduler)
                .observeOn(mainScheduler)
                .subscribe(new Observer<List<SkyResponseModel>>() {
                    @Override
                    public void onCompleted() {
                        view.onFetchDataCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {

                        view.onFetchDataError(e);
                    }

                    @Override
                    public void onNext(List<SkyResponseModel> rootModel) {
                        view.onFetchDataSuccess(rootModel);
                    }
                });

        subscriptions.add(subscription);

    }

    @Override
    public void subscribe() {
        loadData();
    }

    @Override
    public void unsubscribe() {
        subscriptions.clear();
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}
