package com.example.prigui.skyproject;


import com.example.prigui.skyproject.model.SkyResponseModel;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Indigo on 2/28/17.
 */

public interface CharactersDataSource {

    @GET("Movies")
    Observable<List<SkyResponseModel>> getCharacters();

}
