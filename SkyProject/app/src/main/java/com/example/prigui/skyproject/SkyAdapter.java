package com.example.prigui.skyproject;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.prigui.skyproject.model.SkyResponseModel;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by prigui on 24/07/2018.
 */

public class SkyAdapter extends RecyclerView.Adapter<SkyAdapter.CustomViewHolder> {
    private List<SkyResponseModel> feedItemList;
    private Context mContext;

    public SkyAdapter(Context context, List<SkyResponseModel> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_movie, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        SkyResponseModel feedItem = feedItemList.get(i);

        //Render image using Picasso library
        if (!TextUtils.isEmpty(feedItem.getCoverUrl())) {
            Picasso.with(mContext).load(feedItem.getCoverUrl())
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(customViewHolder.imageView);
        }

        //Setting text view title
        customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected TextView textView;

        public CustomViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.movie_image_iv);
            this.textView = (TextView) view.findViewById(R.id.movie_title_tv);
        }
    }
}
