package com.example.prigui.skyproject;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.prigui.skyproject.model.SkyResponseModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by prigui on 24/07/2018.
 */

public class SkyMainActivity extends AppCompatActivity implements SkyContract.View {

    private SkyContract.Presenter mPresenter;
    private SkyAdapter mAdapter;

    private CharactersDataSource charactersDataSource;

    @BindView(R.id.recyclerview) RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setup();

//        new CharactersRemoteDataSource().getCharacters()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Subscriber<List<SkyResponseModel>>() {
//                    @Override
//                    public void onCompleted() {
//                        Log.e(">>", "AA");
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.e(">>", e.toString());
//                    }
//
//                    @Override
//                    public void onNext(List<SkyResponseModel> skyResponseModel) {
//
//                        Log.e(">>", skyResponseModel.toString());
//
//                    }
//                });

    }

    private void setup() {
       // setRecyclerView();
        setPresenter();
    }

    private void setPresenter() {
        charactersDataSource = new CharactersRemoteDataSource();
        mPresenter = new SkyPresenter(
                charactersDataSource,
                Schedulers.io(),
                AndroidSchedulers.mainThread(),
                this
        );
        mPresenter.subscribe();
        mPresenter.loadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unsubscribe();
    }

    private void setRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new SkyAdapter(this, new ArrayList<SkyResponseModel>(0));
        mRecyclerView.setAdapter(mAdapter);

        // Configurando um dividr entre linhas,.
        mRecyclerView.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onFetchDataStarted() {

    }

    @Override
    public void onFetchDataCompleted() {

    }

    @Override
    public void onFetchDataSuccess(List<SkyResponseModel> skyResponseModel) {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new SkyAdapter(this, skyResponseModel);
        mRecyclerView.setAdapter(mAdapter);

        // Configurando um dividr entre linhas,.
        mRecyclerView.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        //mAdapter = new SkyAdapter(skyResponseModel);
    }

    @Override
    public void onFetchDataError(Throwable e) {

    }
}
