package com.example.prigui.skyproject.model;

import java.util.List;

/**
 * Created by Indigo on 3/30/17.
 */

public class CharacterModelBuilder {

    private String title;
    private String overview;
    private String duration;
    private String release_year;
    private String cover_url;
    private List<String> backdrops_url;
    private String id;


    public CharacterModelBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public CharacterModelBuilder setOverview(String overview) {
        this.overview = overview;
        return this;
    }

    public CharacterModelBuilder setDuration(String duration) {
        this.duration = duration;
        return this;
    }

    public CharacterModelBuilder setRelease_year(String release_year) {
        this.release_year = release_year;
        return this;
    }


    public CharacterModelBuilder setCover_url(String cover_url) {
        this.cover_url = cover_url;
        return this;
    }


    public CharacterModelBuilder setBackdrops_url(List<String> backdrops_url) {
        this.backdrops_url = backdrops_url;
        return this;
    }



    public CharacterModelBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public SkyResponseModel build() {
        return new SkyResponseModel(
            this.title,
            this.overview,
            this.duration,
            this.release_year,
            this.cover_url,
            this.backdrops_url,
            this.id
        );

    }
}
