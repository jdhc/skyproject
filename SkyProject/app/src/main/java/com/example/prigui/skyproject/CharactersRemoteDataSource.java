package com.example.prigui.skyproject;

import com.example.prigui.skyproject.model.SkyResponseModel;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Indigo on 3/1/17.
 */

public class CharactersRemoteDataSource implements CharactersDataSource {

    private CharactersDataSource api;
    private final String URL = "https://sky-exercise.herokuapp.com/api/";

    public CharactersRemoteDataSource() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        this.api = retrofit.create(CharactersDataSource.class);
    }

    @Override
    public rx.Observable<List<SkyResponseModel>> getCharacters() {
        return this.api.getCharacters();
    }
}
