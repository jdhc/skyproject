package com.example.prigui.skyproject;


import com.example.prigui.skyproject.model.SkyResponseModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Indigo on 3/9/17.
 */

public class SkyPresenterTest {

    @Mock
    private CharactersDataSource charactersDataSource;

    @Mock
    private SkyContract.View view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void fetchValidDataShouldLoadIntoView() {

        List<SkyResponseModel> skyResponseModel = new ArrayList<>();

        when(charactersDataSource.getCharacters())
            .thenReturn(Observable.just(skyResponseModel));

        SkyPresenter skyPresenter = new SkyPresenter(
                this.charactersDataSource,
                Schedulers.immediate(),
                Schedulers.immediate(),
                this.view
        );

        skyPresenter.loadData();

        InOrder inOrder = Mockito.inOrder(view);
        inOrder.verify(view, times(1)).onFetchDataStarted();
        inOrder.verify(view, times(1)).onFetchDataSuccess(skyResponseModel);
        inOrder.verify(view, times(1)).onFetchDataCompleted();
    }

    @Test
    public void fetchErrorShouldReturnErrorToView() {

        Exception exception = new Exception();

        when(charactersDataSource.getCharacters())
                .thenReturn(Observable.<List<SkyResponseModel>>error(exception));

        SkyPresenter skyPresenter = new SkyPresenter(
                this.charactersDataSource,
                Schedulers.immediate(),
                Schedulers.immediate(),
                this.view
        );

        skyPresenter.loadData();

        InOrder inOrder = Mockito.inOrder(view);
        inOrder.verify(view, times(1)).onFetchDataStarted();
        inOrder.verify(view, times(1)).onFetchDataError(exception);
        verify(view, never()).onFetchDataCompleted();
    }

}
