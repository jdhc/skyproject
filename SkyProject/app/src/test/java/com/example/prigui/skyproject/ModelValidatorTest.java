package com.example.prigui.skyproject;


import com.example.prigui.skyproject.model.CharacterModelBuilder;
import com.example.prigui.skyproject.model.SkyResponseModel;
import com.example.prigui.skyproject.model.validator.ModelValidator;

import org.junit.Test;

/**
 * Created by Indigo on 3/22/17.
 */

public class ModelValidatorTest {

    @Test()
    public void shouldNotThrowErrorOnValidCharacterModel() throws IllegalArgumentException {
        CharacterModelBuilder builder = new CharacterModelBuilder();
        builder.setTitle("Han Solo");

        SkyResponseModel skyResponseModel = builder.build();

        ModelValidator validator = new ModelValidator(skyResponseModel);
        validator.validate();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowErrorOnInvalidCharacterModel() throws IllegalArgumentException {
        CharacterModelBuilder builder = new CharacterModelBuilder();
        builder.setTitle(null);

        SkyResponseModel skyResponseModel = builder.build();

        ModelValidator validator = new ModelValidator(skyResponseModel);
        validator.validate();
    }
}
